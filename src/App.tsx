import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import { Login } from './Login';

function App() {

    const handleSubmit = (event: any) => {
        event.preventDefault();
        const username = event.target.username.value;
        const password = event.target.password.value;

        postData(username, password);
    };

    const postData = async (username: string, password: string) => {
        try {
            const responseData = await Login({ username: username, password: password })
            console.log('Token: ', responseData.accessToken)
        } catch (error: any) {
            console.error('Error:', error);
        }
    }

      return (
          <div className="container">
              <div className="row">
                  <div className="col-md-6">
                      <div className="card">
                          <form className="box" onSubmit={handleSubmit}>
                              <h1>Login</h1>
                              <p className="text"> Please enter your login and password!</p>
                              <input type="text" name="username" placeholder="Username"/>
                              <input type="password" name="password" placeholder="Password"/>
                              <a className="forgot" href="#">Forgot password?</a>
                              <input type="submit" name="" value="Login" />
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      );
}

export { App };