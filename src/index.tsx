import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './ReportWebVitals';
import { App }  from './App'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
    <App/>
)

reportWebVitals();
