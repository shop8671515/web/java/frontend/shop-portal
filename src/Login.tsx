import { AccessTokenCreateRequest } from '@ryszardszewczyk2204/auth';
import { DefaultHeaders } from './Headers';

async function Login(request: AccessTokenCreateRequest) {

    const requestOptions: RequestInit = {
        method: 'POST',
        headers: DefaultHeaders(),
        body: JSON.stringify({ username: request.username, password: request.password })
    };

    const responseData = await (await fetch('http://localhost:58032/tokens', requestOptions)).json();
    return responseData;
}

export { Login };