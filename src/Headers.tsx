function DefaultHeaders() {
    return {
        'Content-Type': 'application/json',
        'Accept': '*/*',
        'Connection': 'keep-alive'
    }
}

export { DefaultHeaders }